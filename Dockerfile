FROM python:3-alpine

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ENV GUNICORN_WORKERS 1
# Replace this with the file with the webhook to use
ENV DISCORD_WEBHOOK_FILE 'invalid'
EXPOSE 80

COPY . .

CMD ["sh", "-c", "gunicorn --threads 4 -w $GUNICORN_WORKERS -b 0.0.0.0:8000 wsgi:app"]
