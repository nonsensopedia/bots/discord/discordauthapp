import os
import json
import traceback

from discord_webhook import DiscordWebhook
from flask import Flask, render_template, request


class PrefixMiddleware:
    def __init__(self, wsgi_app, prefix=''):
        self.app = wsgi_app
        self.prefix = prefix

    def __call__(self, environ, start_response):
        if environ['PATH_INFO'].startswith(self.prefix):
            environ['PATH_INFO'] = environ['PATH_INFO'][len(self.prefix):]
            environ['SCRIPT_NAME'] = self.prefix
            return self.app(environ, start_response)
        start_response('404', [('Content-Type', 'text/plain')])
        return ['This seems wrong.'.encode()]


app = Flask(__name__)
app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix='/auth')
webhook_url = open(os.environ['DISCORD_WEBHOOK_FILE'], 'r').read().strip()


# noinspection PyBroadException
@app.route('/a/<discord_id>')
def main(discord_id):
    """
    :param discord_id: Decimal Discord user ID
    """
    try:
        discord_id = int(discord_id)
        payload = f'!webhook <@{discord_id}>\n' + '`' + json.dumps({
            'discord_id': discord_id,
            'oauth_verifier': request.args['oauth_verifier'],
            'oauth_token': request.args['oauth_token'],
        }) + '`'
        webhook = DiscordWebhook(url=webhook_url, content=payload)
        response = webhook.execute()
        if response.status_code not in [200, 204]:
            raise Exception(
                f"failure response status code {response.status_code}"
            )
    except Exception:
        traceback.print_exc()
        return render_template('error.html')
    else:
        return render_template('good.html')
